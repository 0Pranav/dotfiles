#Emulate cls from Windows DOS
alias cls=clear

#Set a default commnad line text editor for bash
text_editor=vim

#Open different config files
alias cfgi3="$text_editor ~/.i3/config"
alias cfgbash="$text_editor ~/.bashrc"
alias cfgalias="$text_editor ~/.bash_aliases"
alias cfgi3b="$text_editor ~/.config/i3blocks/config"
alias reloadbash=". ~/.bashrc"
alias cfgpb="$text_editor ~/.config/polybar/config"

#Directory bookmarks and shortcuts
alias ..="cd .."
alias srcs="cd ~/srcs"
alias downloads="cd ~/Downloads"
alias ~='cd ~'
#Updating software
# alias lu="sudo apt update && apt list --upgradeable"
# alias iu="sudo apt upgrade" 

# use vim instead of vi
alias vi=vim
#screenshot
alias takess="scrot -d 5 -s ~/screenshots/foo.png && xclip ~/screenshots/foo.png && rm ~/foo.png"
